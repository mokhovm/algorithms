#include <stdbool.h>

/**
 * \brief ������ ���, ���� ������� ������
 * \return 
 */
bool q_isFull();


/**
 * \brief ������ ���, ���� ������� ������
 * \return 
 */
bool q_isEmpty();

/**
 * \brief ������ ����� �������
 * \return 
 */
int q_length();

/**
 * \brief ��������� ������� � �������
 * \param value 
 */
void q_push(int value);

/**
 * \brief ��������� ������� �� �������
 * \return 
 */
int q_pop();

/**
 * \brief ������������� ������� �������
 * \return 
 */
int q_peek();
