#ifndef SIMPLEPROF_H
#define SIMPLEPROF_H
#include <time.h>


/**
 * \brief ����� ����������
 * \param a_time ����������, �������� ����� ������. 
 * ����� �������� NULL ��� �������� ������ ������ ������
 */
void sp(clock_t *a_time);

/**
 * \brief ���� ����������
 * \param a_time ����������, �������� ����� ������. 
 * ����� �������� NULL ��� �������� ������ ������ ������
 * \return ������ ����� ����� ������ ����������
 */
clock_t ep(clock_t *a_time);


/**
 * \brief ������� � ������� ���������� � ������� ������ ����������
 * \param a_time ����������, �������� ����� ������. 
 * ����� �������� NULL ��� �������� ������ ������ ������
 */
void epp(clock_t *a_time);

#endif // SIMPLEPROF_H
