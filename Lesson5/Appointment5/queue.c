#include <stdbool.h>
#define MAX_LEN 10
#define HEAD_START 0
#define TAIL_START -1

int _q_array[MAX_LEN];
int _q_head = HEAD_START;
int _q_tail = TAIL_START;
int _q_count = 0;

bool q_isFull()
{
	return _q_count == MAX_LEN;
}

bool q_isEmpty()
{
	return _q_count == 0;
}

int q_length() {
	return _q_count;
}

void q_push(int value)
{
	if (!q_isFull())
	{
		if (_q_tail == MAX_LEN - 1) {
			_q_tail = TAIL_START;
		}

		_q_array[++_q_tail] = value;
		_q_count++;
	}
}

int q_pop()
{
	int res = _q_array[_q_head++];

	if (_q_head == MAX_LEN) {
		_q_head = HEAD_START;
	}

	_q_count--;
	return res;
}


int q_peek()
{
	return _q_array[_q_head];
}


