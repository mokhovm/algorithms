#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "stack.h"


bool is_left_brace(char brace)
{
	return (brace == '(' || brace == '{' || brace == '[' || brace == '<');
}

bool is_right_brace(char brace)
{
	return (brace == ')' || brace == '}' || brace == ']' || brace == '>');
}

char get_left_brace_pair(char left_brace)
{
	if (left_brace == '(') return ')';
	else if (left_brace == '{') return '}';
	else if (left_brace == '[') return ']';
	else if (left_brace == '<') return '>';
	else NULL;
}

char get_right_brace_pair(char rigth_brace)
{
	if (rigth_brace == ')') return '(';
	else if (rigth_brace == '}') return '{';
	else if (rigth_brace == ']') return '[';
	else if (rigth_brace == '>') return '<';
	else NULL;
}

bool checkBraces(char str[])
{
	bool res = true;
	int i = 0;
	Stack myStack = stack_create();
	while (str[i] != '\0') {
		char c = str[i];
		//printf("������ %c \n", c);
		if (is_left_brace(c))
		{
			stack_push(&myStack, c);
		}
		else if (is_right_brace(c))
		{
			
			if (stack_is_empty(&myStack))
			{
				res = false;
				break;
			}
			

			char data = stack_pop(&myStack);
			if (c != get_left_brace_pair(data))
			{
				res = false;
				break;
			}
			
		}
		else
		{
			res = false;
			break;
		}
		i++;
	}

	// ���� � ����� �������� �������, �� ���-�� �� ���
	if (res && stack_len(&myStack) > 0) res = false;
	 
	return res;


	//
	//int good_braces_succession = 1;
	//Stack open_braces = stack_create();

	//while (brace != '\n') {
	//	if (check_left_brace(brace)) {
	//		stack_push(&open_braces, brace);
	//	}
	//	else if (check_right_brace(brace)) {
	//		if (stack_is_empty(&open_braces)) {
	//			good_braces_succession = 0;
	//			break;
	//		}
	//		char pair_candidate = stack_pop(&open_braces);
	//		if (get_the_brace_left_pair(brace) != pair_candidate) {
	//			good_braces_succession = 0;
	//			break;
	//		}
	//	} //else ������ ������ �� ������

	//	brace = getchar();
	//}
	//if (good_braces_succession
	//	&& !stack_is_empty(&open_braces)) {
	//	//�������� ���������� ����� ������
	//	good_braces_succession = 0;
	//}

	//if (good_braces_succession) {
	//	printf("OK!\n");
	//}
	//else {
	//	printf("Bad succession!\n");
	//}
}