#include <stdlib.h>

typedef char StackData;

typedef struct StackNode {
	StackData value;
	struct StackNode *next;
} tNode;

typedef struct {
	tNode *p_begin;
	size_t m_size;
} Stack;

void stack_push(Stack *s, StackData value);
StackData stack_pop(Stack *s);
StackData stack_top(const Stack *s);
size_t stack_len(const Stack *s);
void stack_clear(Stack *s);
Stack stack_create();
void stack_print(const Stack *s);
int stack_is_empty(const Stack *s);
