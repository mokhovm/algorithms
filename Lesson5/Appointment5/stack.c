#include "stack.h"
#include <stdio.h>
#include <assert.h>

//����������� ����������� ������
void print_list(tNode *p_begin)
{
	tNode *p = p_begin;
	while (p != NULL) {
		//����������� ��������� ������
		printf("%c \t", p->value);
		//������� ������ !!!
		p = p->next;
	}
}

//�������� ������ ����������� ������
void delete_list(tNode *p_begin)
{
	tNode *p = p_begin;
	while (p != NULL) {
		tNode *tmp;
		tmp = p;
		//������� ������ !!!
		p = p->next;
		//������� ������ ������
		free(tmp);
	}
}

void stack_push(Stack *s, StackData value)
{
	tNode *p = (tNode *)malloc(sizeof(tNode));
	p->value = value;
	p->next = s->p_begin;
	s->p_begin = p;
	s->m_size++;
}

StackData stack_pop(Stack *s)
{
	//??? � ���, ���� ���� ���� ???
	tNode *tmp = s->p_begin;
	StackData tmp_value = tmp->value;
	s->p_begin = s->p_begin->next;
	s->m_size--;
	free(tmp);
	return tmp_value;
}

StackData stack_top(const Stack *s)
{
	//??? � ���, ���� ���� ���� ???
	return s->p_begin->value;
}

size_t stack_len(const Stack *s)
{
	return s->m_size;
}

void stack_clear(Stack *s)
{
	delete_list(s->p_begin);
	s->p_begin = NULL;
	s->m_size = 0;
}

Stack stack_create()
{
	Stack new_stack = { NULL, 0 };
	return new_stack;
}

void stack_print(const Stack *s)
{
	print_list(s->p_begin);
}

int stack_is_empty(const Stack *s)
{
	return s->m_size == 0;
}