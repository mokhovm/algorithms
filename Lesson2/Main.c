#include "Main.h"
#include "stdio.h"
#include "windows.h"
#include "stdbool.h"


/*
  �������� ������� �2 �� ����� ��������� � ��������� ������.
  �������� ����� ������.

  1. ����������� ������� �������� �� 10 ������� � �������� ��������� ��������.

  2. ����������� ������� ���������� ����� a � ������� b:

  a. ��� ��������;
  b. ����������;
  c. *����������, ��������� �������� �������� �������.
  3. ����������� ����������� ����������� ����� �����, ���������� �� ������. � ����������� ��� �������, ������ ������� �������� �����:

  ������� 1
  ������ �� 2
  ������ ������� ����������� ����� �� ������ �� 1, ������ ����������� ��� ����� � 2 ����. ������� ���������� ��������, ������� ����� 3 ����������� � ����� 20.
  �) � �������������� �������;
  �) � �������������� ��������.
  ����������� ���� � ������� ������� ���������� �������: �� �����, ���������� �������, � ����������.
*/

/*
���������� ����� �����
*/
void flush()
{
	char c;
	while ((c = getchar()) != '\n' && c != EOF);
}

/*
����������� ������� ��� �������� ����� �� ���������� � �������� �������
*/
void IntToBin(int n)
{
	int d = n / 2;
	if (d > 0)
		IntToBin(d);
	else
		printf("0b");
	printf ("%d", n % 2);
}

//char * IntToBin2(int n)
//{
//	char res[50] = {""};
//	int d = n / 2;
//	if (d > 0)
//		*res = IntToBin2(d);
//	else
//		*res = strcat_s(res, _countof(res), "0b");
//
//	
//	char c[10];
//	c[0] = (char)(n % 2);
//	*res = strcat_s(res, _countof(res), "aa");
//}

/*
������� 1
����������� ������� �������� �� 10 ������� � �������� ��������� ��������.
*/
void solution1()
{
	printf("����������� ������� �������� �� 10 ������� � �������� ��������� ��������.\n");
	int num;
	printf("������� ����� � ���������� �������: ");
	scanf_s("%d", &num);
	printf("� �������� ������� ��� ����� ����� �������� ���: ");
	IntToBin(num);
	printf("\n");
	flush();
}

/*
������-������������ �����
*/
long int pow1(long int a, unsigned int p)
{
	int res = 1;
	for (int i = 0; i < p; i++)
	{
		res *= a;
	}
	return res;
}

/*
������������� �����
*/
long int pow2(long int a, unsigned int p)
{
	int res;
	res = (p > 0) ? a * pow2(a, p - 1) : 1;
	return res;
}

/*
������������ ����� 
*/
long int pow3(long int a, unsigned int p)
{
	int res;

	if (p == 0)
		res = 1;
	else if (p % 2 == 0)
		res = pow3(a * a, p / 2);
	else
		res = pow3(a * a, p / 2) * a;
	return res;
}


/*
������� 2
����������� ������� ���������� ����� a � ������� b:

a. ��� ��������;
b. ����������;
c. *����������, ��������� �������� �������� �������.
*/
void solution2()
{
	printf("����������� ������� ���������� ����� a � ������� b .\n");
	int num;
	int p;

	printf("������� �����: ");
	scanf_s("%d", &num);
	printf("������� �������: ");
	scanf_s("%d", &p);
	printf("������� 1: %d \n", pow1(num, p));
	printf("������� 2: %d \n", pow2(num, p));
	printf("������� 3: %d \n", pow3(num, p));

}



int task3a(int from, int to) {
	int *arr = calloc((size_t)to, sizeof(int));
	arr[from] = 1;

	for (int i = from + 1; i <= to; ++i) {
		if (i % 2) {
			arr[i] = arr[i - 1];
		}
		else {
			arr[i] = (arr[i - 1] + arr[i / 2]);
		}
	}
	int result = arr[to];
	free(*arr);
	return result;
}


int task3b(int from, int to) {
	if (to == 0) return 0;
	if (to == from) return 1;
	if (to % 2) {
		return task3b(from, to - 1);
	}
	else {
		return task3b(from, to / 2) + task3b(from, to - 1);
	}
}


/*
������� 3
3. ����������� ����������� ����������� ����� �����, ���������� �� ������. � ����������� ��� �������, ������ ������� �������� �����:

������� 1
������ �� 2
������ ������� ����������� ����� �� ������ �� 1, ������ ����������� ��� ����� � 2 ����. ������� ���������� ��������, ������� ����� 3 ����������� � ����� 20.
�) � �������������� �������;
�) � �������������� ��������.
����������� ���� � ������� ������� ���������� �������: �� �����, ���������� �������, � ����������.
*/
void solution3()
{
	printf("���������� � ������� �������: %d\n", task3a(3, 20));
	printf("���������� � ������� ��������: %d\n", task3b(3, 20));
}

/*
�������� ��������� ���� ������
*/
void printMenu()
{
	printf("\n***���� �����***\n");
	printf("�������� �������:\n");
	printf("1 - ������� �� ���������� � �������� �������\n");
	printf("2 - ���������� ����� a � ������� b\n");
	printf("3 - '�����������'\n");
	printf("0 - exit\n");
}

/*
���������� ���� ���������
*/
void handleMenu()
{

	int val;
	do
	{
		printMenu();
		scanf_s("%i", &val);
		switch (val)
		{
		case 1:
			solution1();
			break;
		case 2:
			solution2();
			break;
		case 3:
			solution3();
			break;

		default:
			break;
		}
	} while (val != 0);

}

int main(int argc, char *argv[])
{
	SetConsoleOutputCP(1251);
	handleMenu();
	getchar();
	return 0;
}

