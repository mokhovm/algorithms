#include "Main.h"
#include "stdio.h"
#include "windows.h"
#include "stdbool.h"
#include "time.h"
#include "simpleProf.h"


/*
  �������� ������� �4 �� ����� ��������� � ��������� ������.
  �������� ����� ������.
    
  1. *���������� ��������� � �������������. ����������� ������ ������� � ������������ � ���������� ���������� ���������.

  ��������, �����:
  3 3
  1 1 1
  0 1 0
  0 1 0

  2. ������ ������ � ���������� ����� ������������ ������������������ � ������� �������.

  3. ***��������� ������ ����� ��������� ����� �������� NxM, ������ ����� ��� ���� ����� �� ������ ����. 
  ����� �������� ������� ����� �� ��� � � ������ � 8 ������. ������� ������ � �������� ��������� ����.


*/

#define N 8
#define M 8
typedef int Board[N][M];



Board blocks;


void Print2(int n, int m, Board a)
{
	int i, j;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
			printf("%6d", a[i][j]);
		printf("\n");
	}
	printf("\n");
}

// ���������� ���������
void Route()
{
	Board board1;

	int i, j;
	for (j = 0; j < M; j++)
		board1[0][j] = 1; // ������ ������ ��������� ���������
	for (i = 1; i < N; i++)
	{
		board1[i][0] = 1;
		for (j = 1; j < M; j++)
			board1[i][j] = board1[i][j - 1] + board1[i - 1][j];
	}
	Print2(N, M, board1);

}

// ������������� ����������
void initBlocks()
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
		{
			int prob = randFromTo(0, 100);
			blocks[i][j] = prob < 10 ? 1 : 0;
		}
	}
}

// ���������� ���������� ��������� � �������������
void RouteWithBlock()
{
	Board board2;
	initBlocks(blocks);
	printf("������ ������ \n");
	Print2(N, M, blocks);
	int i, j;
	int val1 = 1, val2 = 1;

	board2[0][0] = 0;

	// �������� ������ �������
	for (i = 1; i < N; i++)
	{
		if (blocks[i][0] == 1) val1 = 0;
		board2[i][0] = val1;
	}

	// �������� ������ �������
	for (j = 1; j < M; j++)
	{
		if (blocks[0][j] == 1) val2 = 0;
		board2[0][j] = val2;
	}

	for (i = 1; i < N; i++)
	{
		for (j = 1; j < M; j++)
		{
			board2[i][j] = blocks[i][j] == 1 ? 0 : board2[i][j - 1] + board2[i - 1][j];
		}
	}
	printf("���������� ���������: \n");
	Print2(N, M, board2);
}




/*
���������� ����� �����
*/
void flush()
{
	char c;
	while ((c = getchar()) != '\n' && c != EOF);
}


/*
������� 1
*���������� ��������� � �������������. ����������� ������ ������� � ������������ � ���������� ���������� ���������.
*/
void solution1()
{
	unsigned int    number;
	rand_s(&number);
	//Route();
	RouteWithBlock();
}

// ���������� ����� ������������������
void largestCommonSubsequence()
{
	#define A 10
	#define B 9

	char word1[] = "geekbrains";
	char word2[] = "geekminds";

	/*int len1 = strlen(word1);
	int len2 = strlen(word2);*/

	typedef int Table[A][B];
	Table table;

	//int *table[len1][len2];

	for (int  i = 1; i < A; i++)
	{
		table[i][0] = 0;
	}

	for (int j = 0; j < B; j++)
	{
		table[0][j] = 0;
	}


	for (int i = 1; i < A; i++)
	{
		if (word1[i] == '\0') break;

		for (int j = 1; j < B; j++)
		{
			if (word2[j] == '\0') break;
			if (word1[i] == word2[j])
			{
				table[i][j] = table[i - 1][j - 1] + 1;
			}
			else
			{
				table[i][j] = table[i - 1][j] >= table[i][j - 1] ? table[i - 1][j] : table[i][j - 1];
			}
		}
	}

	Print2(A, B, table);


}

/*
������� 2
������ ������ � ���������� ����� ������������ ������������������ � ������� �������.
*/
void solution2()
{
	largestCommonSubsequence();
}


/*
������� 3
***��������� ������ ����� ��������� ����� �������� NxM, ������ ����� ��� ���� ����� �� ������ ����.
����� �������� ������� ����� �� ��� � � ������ � 8 ������. ������� ������ � �������� ��������� ����.
*/
void solution3()
{
}


/*
�������� ��������� ���� ������
*/
void printMenu()
{
	printf("\n***���� �����***\n");
	printf("�������� �������:\n");
	printf("1 - ���������� ��������� � �������������\n");
	printf("2 - ������������ ������������������ � ������� �������\n");
	printf("3 - ����� ����� ��������� �����\n");
	printf("0 - exit\n");
	printf(">");
}

/*
���������� ���� ���������
*/
void handleMenu()
{

	int val;
	do
	{
		printMenu();
		scanf_s("%i", &val);
		switch (val)
		{
		case 1:
			solution1();
			break;
		case 2:
			solution2();
			break;
		case 3:
			solution3();
			break;

		default:
			break;
		}
		flush();
	} while (val != 0);

}

int main(int argc, char *argv[])
{
	SetConsoleOutputCP(1251);
	handleMenu();
	getchar();
	return 0;
}

