#include "Main.h"
#include "stdio.h"
#include "windows.h"
#include "stdbool.h"


/*
  �������� ������� �1 �� ����� ��������� � ��������� ������.
  �������� ����� ������.
*/

/*
���������� ����� �����
*/
void flush()
{
	char c;
	while ((c = getchar()) != '\n' && c != EOF);
}


/*
������� 1
������ ������� ����� ����.
*/
void solution1()
{
	printf("������ ��� � ���� ��������. ���������� � ������� ������ ����� ���� �� "
		"������� I=m/(h*h); ��� m - ����� ���� � �����������, h - ���� � ������.\n");
	double growth;
	double weight;
	double index;

	printf("������� ��� � �����������: ");
	scanf_s("%lf", &weight);
	printf("������� ���� � ������: ");
	scanf_s("%lf", &growth);
	index = weight / (growth * growth);
	printf("������ ����� ���� ���������� %lf\n", index);
	flush();
}


/*
������ ����� � ������� ���. ����������
*/
swapInt1(int *a, int *b)
{
	int c = *a;
	*a = *b;
	*b = c;
}

/*
������ ����� ��� ������������� �������������� ������
*/
swapInt2(int *a, int *b)
{
	*a = *a + *b;
	*b = *a - *b;
	*a = *a - *b;
}

/*
������� 2
����� �����
*/
void solution2()
{
	printf("�������� ��������� ������ ���������� ���� ������������� ����������: \n"
		"a.� �������������� ������� ����������; \n"
		"b. *��� ������������� ������� ����������. \n");
	int a;
	int b;
	printf("������� �������� ������ ����������: \n");
	scanf_s("%d", &a);
	printf("������� �������� ������ ����������: \n");
	scanf_s("%d", &b);
	swapInt1(&a, &b);
	printf("���������� ����� 1: \n");
	printf("�������� ������ ����������:%d \n", a);
	printf("�������� ������ ����������:%d \n", b);
	swapInt2(&a, &b);
	printf("���������� ����� 2: \n");
	printf("�������� ������ ����������:%d \n", a);
	printf("�������� ������ ����������:%d \n", b);
	flush();
}

/*
��������� ����� �� �������������
*/
bool checkAutomorf(int i)
{
	// ������� �����
	int sqr = i * i;
	// ���������� ��������
	int div = 10;
	// ������ ���������� �������� ��� �����
	while (div <= i) div *= 10;
	// ���� ������� �� ������� ����� ����� ������ �����, �� ��� �����������
	return sqr % div == i;
}

/*
������� 3
����������� �����
*/
void solution3()
{
	printf("* ����������� �����. ����������� ����� ���������� �����������, ���� ��� ����� "
		"��������� ������ ������ ��������.��������, 25^2 = 625. �������� ���������, ������� "
		"������ ����������� ����� N � ������� �� ����� ��� ����������� �����, �� ������������� N.\n");

	int limit;
	printf("������� ����������� �����, �� �������� ��������� ����� ������ ����������� �����:");
	scanf_s("%d", &limit);
	printf("��������� ����������� �����:\n");
	for (int i = 0; i < limit; i++)
	{
		if (checkAutomorf(i)) printf("%d \n", i);
	}

}

/*
�������� ��������� ���� ������
*/
void printMenu()
{
	printf("\n***���� �����***\n");
	printf("�������� �������:\n");
	printf("1 - ������ ����� ����\n");
	printf("2 - ����� ����� �����\n");
	printf("3 - ����������� �����\n");
	printf("0 - exit\n");
}

/*
���������� ���� ���������
*/
void handleMenu()
{

	int val;
	do
	{
		printMenu();
		scanf_s("%i", &val);
		switch (val)
		{
		case 1:
			solution1();
			break;
		case 2:
			solution2();
			break;
		case 3:
			solution3();
			break;

		default:
			break;
		}
	} while (val != 0);

}

int main(int argc, char *argv[])
{
	SetConsoleOutputCP(1251);
	handleMenu();
	getchar();
	return 0;
}

