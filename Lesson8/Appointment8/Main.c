#define _CRT_SECURE_NO_WARNINGS

#include "Main.h"
#include "stdio.h"
#include "windows.h"
#include "stdbool.h"
#include "time.h"
#include "simpleProf.h"




/*
  �������� ������� �8 �� ����� ��������� � ��������� ������.
  �������� ����� ������.

  1. ����������� ���������� ���������.
  2. ����������� ������� ����������.
  3. *����������� ���������� ��������.
  4. **����������� �������� ���������� �� �������

*/


#define LEN 20
#define MAX_VALUE 100

int sampleArray[LEN];
int workArray[LEN];



/*
���������� ����� �����
*/
void flush()
{
	char c;
	while ((c = getchar()) != '\n' && c != EOF);
}


/*
 * ���������� ��������� � ��������� ��������
 */
int countingSort(int *arr, const int len) 
{

	int res = 0;
	int freq[MAX_VALUE];


	for (int i = 0; i < MAX_VALUE; i++) 
	{
		freq[i] = 0;
		res++;
	}

	for (int i = 0; i < len; i++) 
	{
		freq[arr[i]]++;
		res++;
	}

	int pos = 0;

	for (int i = 0; i <= MAX_VALUE; i++) 
	{
		for (int j = 0; j < freq[i]; j++) 
		{
			arr[pos++] = i;
			res++;
		}
	}
	return res;
}

/*
������ ����������
*/
int quickSort(int *arr, int first, int last)
{
	int res = 0;
	if (first < last)
	{
		int left = first, right = last, mid = arr[(left + right) / 2];
		do
		{
			while (arr[left] < mid) left++;
			while (arr[right] > mid) right--;
			if (left <= right)
			{
				int tmp = arr[left];
				arr[left] = arr[right];
				arr[right] = tmp;
				left++;
				right--;
				res++;
			}
		} 
		while (left <= right);

		res += quickSort(arr, first, right);
		res += quickSort(arr, left, last);
	}
	return res;
}


/*
 * ���������� ����� �������.
 * arr - �������������� ������
 * leftArr - ����� ����� �������
 * leftCnt - ���������� ������� � ����� �������
 * rightArr - ������ ����� �������
 * rightCnt - ���������� ������� � ������ �������
 */
int merge(int *arr, int *leftArr, int leftCnt, int *rightArr, int rightCnt) 
{
	int res = 0;
	// ������ ������ �������
	int leftIdx = 0;
	// ������ ������� �������
	int rightIdx = 0;
	// ������ ��������������� �������
	int allIdx = 0;


	while (leftIdx < leftCnt && rightIdx < rightCnt) 
	{
		if (leftArr[leftIdx]  < rightArr[rightIdx]) 
			arr[allIdx++] = leftArr[leftIdx++];
		else 
			arr[allIdx++] = rightArr[rightIdx++];
		res++;
	}

	while (leftIdx < leftCnt)
	{
		arr[allIdx++] = leftArr[leftIdx++];
		res++;
	}

	while (rightIdx < rightCnt)
	{
		arr[allIdx++] = rightArr[rightIdx++];
		res++;
	}
	return res;
}

/*
 * ���������� ��������
 */
int mergeSort(int *arr, int len) 
{
	int res = 0;
	int mid, i, *leftArr, *rightArr;
	if (len > 1)
	{
		mid = len / 2;  

		// �������� ����� � ������ ����������
		leftArr = (int*)malloc(mid * sizeof(int));
		rightArr = (int*)malloc((len - mid) * sizeof(int));

		// � �������� �� ��������� ������� �� �������
		for (i = 0; i < mid; i++)
		{
			leftArr[i] = arr[i]; 
			res++;
		}
		for (i = mid; i < len; i++)
		{
			rightArr[i - mid] = arr[i]; 
			res++;
		}

		// �� ������� ����������� ������ �� ��������
		res += mergeSort(leftArr, mid);  
		res += mergeSort(rightArr, len - mid);  
		res += merge(arr, leftArr, mid, rightArr, len - mid);  
		free(leftArr);
		free(rightArr);
	}
	return res;
}


/* ���������� ������� */
int pigeonholeSort(int *arr, const int len)
{
	int res = 0;
	int min = 0, max = 0;

	// ������ ����������� � ������������ �������� � �������
	for (int i = 0; i < len; i++)
	{
		if (arr[i] < min)
		{
			min = arr[i];
		}
		if (arr[i] > max)
		{
			max = arr[i];
		}
		res++;
	}

	int size = max - min + 1;

	int *holes = (int*)calloc(size, sizeof(int));

	//for (int i = 0; i < size; i++)
	//{
	//	holes[i] = 0;
	//	res++;
	//}

	for (int i = 0; i < len; i++)
	{
		holes[arr[i] - min]++;
		res++;
	}

	int index = 0;

	for (int i = 0; i < size; i++)
	{
		while (holes[i]-- > 0)
		{
			arr[index++] = i + min;
			res++;
		}
	}
	free(holes);
	return res;
}

/*
������� 1
*/
void solution1()
{
	printf("���������� ��������� \n");

	memcpy(&workArray, &sampleArray, sizeof(sampleArray));
	printarray(workArray, LEN);
	sp(NULL);
	printf("���������� ��������: %d \n", countingSort(workArray, LEN));
	epp(NULL);
	printarray(workArray, LEN);


}

/*
������� 2
*/
void solution2()
{
	printf("������� ���������� \n");

	memcpy(&workArray, &sampleArray, sizeof(sampleArray));
	printarray(workArray, LEN);
	sp(NULL);
	printf("���������� ��������: %d \n", quickSort(workArray, 0, LEN - 1 ));
	epp(NULL);
	printarray(workArray, LEN);
}


/*
������� 3
*/
void solution3()
{
	
	printf("���������� ��������\n");

	memcpy(&workArray, &sampleArray, sizeof(sampleArray));
	printarray(workArray, LEN);
	sp(NULL);
	printf("���������� ��������: %d \n", mergeSort(workArray, LEN));
	epp(NULL);
	printarray(workArray, LEN);
}


/*
������� 4
*/
void solution4()
{

	printf("���������� �� �������\n");

	memcpy(&workArray, &sampleArray, sizeof(sampleArray));
	printarray(workArray, LEN);
	sp(NULL);
	printf("���������� ��������: %d \n", pigeonholeSort(workArray, LEN));
	epp(NULL);
	printarray(workArray, LEN);
}


/*
�������� ��������� ���� ������
*/
void printMenu()
{
	printf("\n***���� �����***\n");
	printf("�������� �������:\n");
	printf("1 - ���������� ���������\n");
	printf("2 - ������� ����������\n");
	printf("3 - ���������� ��������\n");
	printf("4 - ���������� �� �������\n");
	printf("0 - exit\n");
	printf(">");
}

/*
���������� ���� ���������
*/
void handleMenu()
{

	int val;
	do
	{
		printMenu();
		scanf_s("%i", &val);
		switch (val)
		{
		case 1:
			solution1();
			break;
		case 2:
			solution2();
			break;
		case 3:
			solution3();
			break;
		case 4:
			solution4();
			break;

		default:
			break;
		}
		flush();
	} while (val != 0);

}

int main(int argc, char *argv[])
{
	fill_array(sampleArray, LEN);
	SetConsoleOutputCP(1251);
	handleMenu();
	return 0;
}

