#include "Main.h"
#include "stdio.h"
#include "windows.h"
#include "stdbool.h"
#include "time.h"
#include "simpleProf.h"


/*
  �������� ������� �3 �� ����� ��������� � ��������� ������.
  �������� ����� ������.

  
  1. ����������� �������������� ����������� ����������. �������� ���������� �������� ��������� 
  ���������������� � �� ���������������� ���������. �������� ������� ����������, ������� 
  ���������� ���������� ��������.

  2. *����������� ��������� ����������.

  3. ����������� �������� �������� ������ � ���� �������, ������� ���������� ��������������� ������. 
  ������� ���������� ������ ���������� �������� ��� -1, ���� ������� �� ������.

  4. *���������� ���������� �������� ��� ������ �� ���������� � �������� ��� 
  � ��������������� ���������� ���������.


*/

#define LEN 20000

int sampleArray[LEN];
int workArray[LEN];


/*
���������� ����� �����
*/
void flush()
{
	char c;
	while ((c = getchar()) != '\n' && c != EOF);
}

/*
����������� ���������� ��������
*/
int bubblesort1(int *arr, const int len) {
	int res = 0;
	for (int i = 0; i < len; i++) {
		for (int j = 0; j < len - 1; j++) {
			res++;
			if (arr[j] > arr[j + 1]) swap(&arr[j], &arr[j + 1]);
		}
	}
	return res;
}

/*
����������� ���������� ��� ������� �� ������� ����� �������
*/
int bubblesort2(int *arr, const int len) {
	int res = 0;
	for (int i = 0; i < len; i++) {
		for (int j = 0; j < len - i - 1; j++) {
			res++;
			if (arr[j] > arr[j + 1]) swap(&arr[j], &arr[j + 1]);
		}
	}
	return res;
}


/*
����������� ���������� ��� ������� �� ������� ����� ������� � ������ ����������, ���� ������ ������������
*/
int bubblesort3(int *arr, const int len) {
	int res = 0;
	for (int i = 0; i < len; i++) {
		bool isSorted = true;
		for (int j = 0; j < len - i - 1; j++) {
			if (arr[j] > arr[j + 1])
			{
				res++;
				swap(&arr[j], &arr[j + 1]);
				isSorted = false;
			}
		}
		if (isSorted) return res;;
	}
	return res;
}

/*
������� 1
1. ����������� �������������� ����������� ����������. �������� ���������� �������� ���������
���������������� � �� ���������������� ���������. �������� ������� ����������, �������
���������� ���������� ��������.
*/
void solution1()
{

	printf("����������� ���������� \n");
	printf("����� 1:\n");

	memcpy(&workArray, &sampleArray, sizeof(sampleArray));
	printarray(workArray, LEN);
	sp(NULL);
	printf("���������� ��������: %d \n", bubblesort1(workArray, LEN));
	epp(NULL);
	printarray(workArray, LEN);
	
	printf("����� 2:\n");

	memcpy(&workArray, &sampleArray, sizeof(sampleArray));
	printarray(workArray, LEN);
	sp(NULL);
	printf("���������� ��������: %d \n", bubblesort2(workArray, LEN));
	epp(NULL);
	printarray(workArray, LEN);

	printf("����� 3:\n");

	memcpy(&workArray, &sampleArray, sizeof(sampleArray));
	printarray(workArray, LEN);
	sp(NULL);
	printf("���������� ��������: %d \n", bubblesort3(workArray, LEN));
	epp(NULL);
	printarray(workArray, LEN);

	flush();
}

/**
 * \brief ���������� ��������������
 * \param arr 
 * \param len 
 * \return 
 */
int shackerSort(int *arr, const int len)
{
	int res = 0;

	int leftMarker = 1;
	int rigntMarker = len - 1;

	while (leftMarker <= rigntMarker)
	{
		for (int i = leftMarker; i <= rigntMarker; i++)
		{
			res++;
			if (arr[i - 1] > arr[i]) swap(&arr[i - 1], &arr[i]);
		}
		rigntMarker--;

		for (int i = rigntMarker; i >= leftMarker; i--)
		{
			res++;
			if (arr[i] < arr[i - 1]) swap(&arr[i], &arr[i - 1]);
		}
		leftMarker++;
	}

	return res;
}


/*
������� 2
2. *����������� ��������� ����������.
*/
void solution2()
{
	printf("��������� ���������� \n");

	memcpy(&workArray, &sampleArray, sizeof(sampleArray));
	printarray(workArray, LEN);
	sp(NULL);
	printf("���������� ��������: %d \n", shackerSort(workArray, LEN));
	epp(NULL);
	printarray(workArray, LEN);
	flush();
}


int binSearch(const int value, int *arr, int len)
{
	int res = -1;
	if (len != 0 && value >= arr[0] && value <= arr[len - 1])
	{
		int left = 0;
		int rignt = len;

		while (left < rignt && res == -1)
		{
			int mid = (rignt - left) / 2 + left;

			if (value < arr[mid])
				rignt = mid;
			else if (value > arr[mid])
				left = mid + 1;
			else
				res = mid;
		}

		if (arr[rignt] == value) res = rignt;
	}
	return res;
}


/*
������� 3
3. ����������� �������� �������� ������ � ���� �������, ������� ���������� ��������������� ������.
������� ���������� ������ ���������� �������� ��� -1, ���� ������� �� ������.
*/
void solution3()
{
	printf("�������� ����� \n");
	printf("������� ����������� ������:\n");
	memcpy(&workArray, &sampleArray, sizeof(sampleArray));
	printarray(workArray, LEN);
	sp(NULL);
	printf("���������� ��������: %d \n", shackerSort(workArray, LEN));
	epp(NULL);
	printarray(workArray, LEN);

	int value = workArray[0];
	printf("����� ������ � ��� ����� %d \n", value);
	sp(NULL);
	int index = binSearch(value, workArray, LEN);
	if (index == -1)
		printf("����� %d � ������� �� ������� \n", value);
	else
		printf("����� %d � ������� ������� �� ������� %d \n", value, index);
	epp(NULL);
	flush();
}

/*
�������� ��������� ���� ������
*/
void printMenu()
{
	printf("\n***���� �����***\n");
	printf("�������� �������:\n");
	printf("1 - ����������� ����������� ����������\n");
	printf("2 - ��������� ����������\n");
	printf("3 - �������� �������� ������\n");
	printf("0 - exit\n");
	printf(">");
}

/*
���������� ���� ���������
*/
void handleMenu()
{

	int val;
	do
	{
		printMenu();
		scanf_s("%i", &val);
		switch (val)
		{
		case 1:
			solution1();
			break;
		case 2:
			solution2();
			break;
		case 3:
			solution3();
			break;

		default:
			break;
		}
	} while (val != 0);

}

int main(int argc, char *argv[])
{
	fill_array(sampleArray, LEN);

	SetConsoleOutputCP(1251);
	handleMenu();
	getchar();
	return 0;
}

