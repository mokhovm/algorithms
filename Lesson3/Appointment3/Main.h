#include "stdio.h"
#include <stdlib.h>
#include <memory.h> 


void swap(int *a, int *b) {
	    *a = *a ^ *b;
	    *b = *b ^ *a;
	    *a = *a ^ *b;
}

void printarray(int *arr, int len) {
	if (len < 20)
	{
		printf("[");
		for (int i = 0; i < len; ++i) {
			printf("%d, ", arr[i]);
		}
		printf("]\n");
	}
}

// srand(time(NULL))
void fill_array(int *arr, int len) {
	for (int i = 0; i < len; ++i) {
		arr[i] = rand() % 100;
	}
}


